// Copyright (c) 2022, Frappe Technologies Pvt. Ltd. and Contributors
// MIT License. See LICENSE

frappe.ui.form.on("Role", {
	refresh(frm) {
		if (frm.doc.name === "All") {
			frm.dashboard.add_comment(
				__("Role 'All' will be given to all system + website users."),
				"yellow"
			);
		} else if (frm.doc.name === "Desk User") {
			frm.dashboard.add_comment(
				__("Role 'Desk User' will be given to all system users."),
				"yellow"
			);
		}

		frm.set_df_property("is_custom", "read_only", frappe.session.user !== "Administrator");

		frm.add_custom_button(
			__("Role Permissions Manager"),
			function () {
				frappe.route_options = { role: frm.doc.name };
				frappe.set_route("permission-manager");
			},
			__("Permissions")
		);

		frm.add_custom_button(__("Show Users"), function () {
			frappe.route_options = { role: frm.doc.name };
			frappe.set_route("List", "User", "Report");
		});

		frm.add_custom_button(
			__("Duplicate with permissions"),
			function () {
				frappe.prompt(
					{
						label: __("New Role Name"),
						fieldname: "new_role_name",
						fieldtype: "Data",
					},
					(values) => {
						frappe
							.call({
								method: "duplicate_with_permissions",
								doc: frm.doc,
								args: {
									new_role_name: values.new_role_name,
								},
								freeze: true,
								freeze_message: __("Role creation in progress"),
							})
							.then((r) => {
								if (r.message) {
									frappe.set_route("Form", "Role", r.message);
								}
							});
					}
				);
			},
			__("Permissions")
		);
	},
});
